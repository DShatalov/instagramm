import { createSelector } from '@ngrx/store';

import { selectUsers } from './users.selectors';
import { PostsState } from '../states/posts.state';
import { AppState } from '../states/app.state';
import { UsersState } from '../states/users.state';

export const selectPosts = (state: AppState) => state.postsState;

/* Снаписать метод selecAllPostsWithComments и затем вызывать в postPage
чтобы передовать в post компонент и сдлеать comments component тупым */
export const selectAllPosts = createSelector(
  selectPosts,
  (state: PostsState) => state.entities.posts ? Object.values(state.entities.posts) : [] // Нужны ли везде вопр знак операторы
);

export const selectAllComments = createSelector(
  selectPosts,
  (state: PostsState) => state.entities.comments ? Object.values(state.entities.comments) : []
);

export const selectCommentsByPostId = createSelector(
  selectPosts,
  (state: PostsState, { id }) => state.entities.comments ? Object.values(state?.entities?.comments).filter(val => val.postId === id) : []
);

export const selectAllPostsWithUsers = createSelector(
  selectUsers,
  selectPosts,
  (usersState: UsersState, postsState: PostsState) => {
    const isDataAvailable = postsState.entities.posts && usersState.entities.users;
    return isDataAvailable ? Object.values(postsState?.entities?.posts).map(post => {
      return { ...post, user: usersState.entities.users[post.userId] };
    }) : [];
  }
);

export const selectCommentsWithUsersByPostId = createSelector(
  selectUsers,
  selectPosts,
  (usersState: UsersState, postsState: PostsState, { id }) => {
    const isDataAvailable = postsState.entities.posts && postsState.entities.comments;
    return isDataAvailable ? Object.values(postsState.entities.comments)
      .filter(val => val.postId === id)
      .map(comment => {
        return { ...comment, user: usersState.entities.users[comment.userId] };
      }) : [];
  }
);
