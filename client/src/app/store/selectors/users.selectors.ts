import { createSelector } from '@ngrx/store';

import { AppState } from '../states/app.state';
import { UsersState } from '../states/users.state';

export const selectUsers = (state: AppState) => state.usersState;

export const selectAllUsers = createSelector(
  selectUsers,
  (state: UsersState) => state?.entities?.users ? Object.values(state?.entities?.users) : [] // Нужны ли везде вопр знак операторы
);

export const selectUserById = createSelector(
  selectUsers,
  (state: UsersState, { id }) => state?.entities?.users[id] // Нужны ли везде вопр знак операторы
);
