import { initialUsersState, UsersState } from './users.state';
import { initialPostsState, PostsState } from './posts.state';

export interface AppState {
  postsState: PostsState;
  usersState: UsersState;
}

export const initialAppState: AppState = {
  postsState: initialPostsState,
  usersState: initialUsersState
};

export function getInitialState(): AppState {
  return initialAppState;
}
