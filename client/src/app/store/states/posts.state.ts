import { SchemaResult } from 'src/app/models/schema';

import { NormalizedPosts } from '../../models/posts';

export interface PostsState {
  result: string[];
  entities: NormalizedPosts;
}

export const initialPostsState: PostsState = {
    result: [],
    entities: {
      comments: {},
      posts: {}
    }
};
