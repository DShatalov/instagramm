import { NormalizedUsers } from './../../models/users';

export interface UsersState {
  result: string[];
  entities: NormalizedUsers;
}

export const initialUsersState: UsersState = {
  result: [],
  entities: {
    users: {},
  }
};
