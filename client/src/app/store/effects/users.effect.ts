import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { ToastService } from 'src/app/services/toast.service';

import { UsersService } from './../../services/users.service';
import { EUsersActions } from '../actions/users.actions';

@Injectable()
export class UsersEffects {
  getAllUsers$ = createEffect(() => this.actions$.pipe(
    ofType(EUsersActions.GetAllUsers),
    mergeMap(() => this.usersService.getAllUsers()
      .pipe(
        map(users => ({ type: EUsersActions.GetAllUsersSuccess, users })),
        catchError(() => EMPTY)
      ))
  )
  );

  UpdateUser$ = createEffect(() => this.actions$.pipe(
    ofType(EUsersActions.UpdateUser),
    mergeMap(({ user }) => this.usersService.updateUser(user)
      .pipe(
        map(val => ({ type: EUsersActions.UpdateUserSuccess, user: val })),
        tap(() => this.toastService.showSuccessToast('Profile updated')),
        catchError(() => EMPTY)
      ))
  )
  );


  constructor(
    private actions$: Actions,
    private usersService: UsersService,
    private toastService: ToastService,
  ) { }
}
