import { ToastService } from 'src/app/services/toast.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { CommentsService } from 'src/app/components/comments/comments.service';

import { PostsService } from './../../pages/posts/posts.service';
import { EPostsActions } from './../actions/posts.actions';

@Injectable()
export class PostsEffects {
  getAllPosts$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.GetAllPosts),
    mergeMap(() => this.postsService.getAllPosts()
      .pipe(
        map(posts => ({ type: EPostsActions.GetAllPostsSuccess, posts })),
        catchError(() => EMPTY)
      ))
    )
  );

  addPost$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.AddPost),
    mergeMap(({ post }) => this.postsService.addPost(post)
      .pipe(
        map(val => ({ type: EPostsActions.AddPostSuccess, post: val })),
        tap(() => this.toastService.showSuccessToast('Post added')),
        catchError(() => EMPTY)
      ))
    )
  );

  deletePost$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.DeletePost),
    mergeMap(({ id }) => this.postsService.deletePost(id)
      .pipe(
        map(val => ({ type: EPostsActions.DeletePostSuccess, id: val })),
        tap(() => this.toastService.showSuccessToast('Post deleted')),
        catchError(() => EMPTY)
      ))
    )
  );

  addComment$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.AddComment),
    mergeMap(({ comment }) => this.commentsService.addPost(comment) // rename to add comment
      .pipe(
        map(val => ({ type: EPostsActions.AddCommentSuccess, comment: val })),
        tap(() => this.toastService.showSuccessToast('Comment added')),
        catchError(() => EMPTY)
      ))
  )
  );

  deleteComment$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.DeleteComment),
    mergeMap(({ commentId, postId }) => this.commentsService.deleteComment(commentId, postId)
      .pipe(
        map(val => ({ type: EPostsActions.DeleteCommentSuccess, commentId: val.commentId, postId: val.postId })),
        tap(() => this.toastService.showSuccessToast('Comment deleted')),
        catchError(() => EMPTY)
      ))
    )
  );

  updateComment$ = createEffect(() => this.actions$.pipe(
    ofType(EPostsActions.UpdateComment),
    mergeMap(({ comment }) => this.commentsService.updateComment(comment)
      .pipe(
        map(val => ({ type: EPostsActions.UpdateCommentSuccess, comment: val})),
        tap(() => this.toastService.showSuccessToast('Comment updated')),
        catchError(() => EMPTY)
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private commentsService: CommentsService,
    private toastService: ToastService,
  ) {}
}
