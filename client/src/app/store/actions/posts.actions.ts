import { createAction, props } from '@ngrx/store';
import { SchemaResult } from 'src/app/models/schema';

import { Comment, NormalizedPosts, Post } from '../../models/posts';

export enum EPostsActions {
  GetAllPosts = '[Posts] Get All Posts',
  GetAllPostsSuccess = '[Posts] Get All Posts Success',
  AddPost = '[Posts] Add Post',
  AddPostSuccess = '[Posts] Add Post Success',
  DeletePost = '[Posts] Delete Post',
  DeletePostSuccess = '[Posts] Delete Post Success',
  AddComment = '[Posts] Add Comment',
  AddCommentSuccess = '[Posts] Add Comment Success',
  DeleteComment = '[Posts] Delete Comment',
  DeleteCommentSuccess = '[Posts] Delete Comment Success',
  UpdateComment = '[Posts] Update Comment',
  UpdateCommentSuccess = '[Posts] Update Comment Success',
}

export const getAllPosts = createAction(EPostsActions.GetAllPosts);
export const getAllPostsSuccess = createAction(EPostsActions.GetAllPostsSuccess, props<{posts: SchemaResult<NormalizedPosts>}>());

export const addPost = createAction(EPostsActions.AddPost, props<{post: Post}>());
export const addPostSuccess = createAction(EPostsActions.AddPostSuccess, props<{post: Post}>());

export const deletePost = createAction(EPostsActions.DeletePost, props<{id: string}>());
export const deletePostSuccess = createAction(EPostsActions.DeletePostSuccess, props<{id: string}>());

export const addComment = createAction(EPostsActions.AddComment, props<{comment: Comment}>());
export const addCommentSuccess = createAction(EPostsActions.AddCommentSuccess, props<{comment: Comment}>());

export const deleteComment = createAction(EPostsActions.DeleteComment, props<{commentId: string, postId: string}>());
export const deleteCommentSuccess = createAction(EPostsActions.DeleteCommentSuccess, props<{commentId: string, postId: string}>()); // Comment interface

export const updateComment = createAction(EPostsActions.UpdateComment, props<{comment: Comment}>());
export const updateCommentSuccess = createAction(EPostsActions.UpdateCommentSuccess, props<{comment: Comment}>());
