import { createAction, props } from '@ngrx/store';

import { NormalizedUsers, User } from './../../models/users';
import { SchemaResult } from './../../models/schema';

export enum EUsersActions {
  GetAllUsers = '[Users] Get All Users',
  GetAllUsersSuccess = '[Users] Get All Users Success',
  UpdateUser = '[Users] Update User',
  UpdateUserSuccess = '[Users] Update User Success',
}

export const getAllUsers = createAction(EUsersActions.GetAllUsers);
export const getAllUsersSuccess = createAction(EUsersActions.GetAllUsersSuccess, props<{ users: SchemaResult<NormalizedUsers> }>());

export const updateUser = createAction(EUsersActions.UpdateUser, props<{ user: User }>());
export const updateUserSuccess = createAction(EUsersActions.UpdateUserSuccess, props<{ user: User }>());
