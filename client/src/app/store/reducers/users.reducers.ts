import { createReducer, on } from '@ngrx/store';

import { initialUsersState, UsersState } from './../states/users.state';
import { getAllUsersSuccess, updateUserSuccess } from '../actions/users.actions';

const initialState = initialUsersState;

const reducer = createReducer(initialState,
  on(getAllUsersSuccess, (state, { users }) => ({ ...state, entities: users.entities, result: users.result })),


  on(updateUserSuccess, (state, { user }) => {
    const { users } = state.entities;
    const newUsers = { ...users, [user._id]: user };

    return { ...state, entities: { ...state.entities, users: newUsers } };
  }),
);

export function usersReducer(state: UsersState, action): UsersState {
  return reducer(state, action);
}
