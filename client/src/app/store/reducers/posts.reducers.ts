import { createReducer, on } from '@ngrx/store';

import { initialPostsState, PostsState } from '../states/posts.state';
import {
  addCommentSuccess,
  addPostSuccess,
  deleteCommentSuccess,
  deletePostSuccess,
  getAllPostsSuccess,
  updateCommentSuccess,
} from '../actions/posts.actions';

const initialState = initialPostsState;
// LODASH GET?
const reducer = createReducer(initialState,
  on(getAllPostsSuccess, (state, { posts }) => ({ ...state, entities: posts.entities, result: posts.result })),

  on(addPostSuccess, (state, { post }) => {
    const { posts } = state.entities;
    const newPosts = { ...posts, [post._id]: post };
    const newEntities = { ...state.entities, posts: newPosts };
    return { ...state, entities: newEntities };
  }),

  on(deletePostSuccess, (state, { id }) => {
    const { posts } = state.entities;
    const { [id]: deletedPost, ...restPosts } = posts;
    const newEntities = { ...state.entities, posts: restPosts };
    return { ...state, entities: newEntities };
  }),

  on(addCommentSuccess, (state, { comment }) => { // добавление коммента и добавление ID коммента в post
    const { comments, posts } = state.entities;
    const newComments = { ...comments, [comment._id]: comment };
    const post = posts[comment.postId];
    const newPost = {...post, comments: [...post.comments, comment._id]};
    const newPosts = {...posts,  [post._id]: newPost };
    const newEntities = { ...state.entities, posts: newPosts,  comments: newComments };
    return { ...state, entities: newEntities };
  }),

  on(deleteCommentSuccess, (state, { commentId, postId }) => { // redfactor reducers
    const { comments, posts } = state.entities;
    const {[commentId]: id, ...newComments} = comments;

    const post = posts[postId];
    const newPostsComments = post.comments.filter(x => x !== commentId );
    const newPost = {...post, comments: newPostsComments};
    const newPosts = {...posts,  [post._id]: newPost };
    const newEntities = { ...state.entities, posts: newPosts,  comments: newComments };
    return { ...state, entities: newEntities };
  }),

  on(updateCommentSuccess, (state, { comment }) => { // добавление коммента и добавление ID коммента в post
    const { comments } = state.entities;
    const newComments = { ...comments, [comment._id]: comment };
    const newEntities = { ...state.entities,  comments: newComments };
    return { ...state, entities: newEntities };
  }),
);

export function postsReducer(state: PostsState, action): PostsState {
  return reducer(state, action);
}
