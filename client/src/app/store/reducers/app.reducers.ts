import { ActionReducerMap } from '@ngrx/store';

import { AppState } from '../states/app.state';
import { postsReducer } from './posts.reducers';
import { usersReducer } from './users.reducers';

export const appReducers: ActionReducerMap<AppState, any> = {
  postsState: postsReducer,
  usersState: usersReducer
};
