import { ActionReducer, MetaReducer } from '@ngrx/store';

export function log(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log('state', state);
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<any>[] = [log];
