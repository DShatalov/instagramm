import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { TextFieldModule } from '@angular/cdk/text-field';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PipesModule } from './pipes/pipes.module';
import { AvatarComponent } from './components/avatar/avatar.component';
import { HeaderComponent } from './components/header/header.component';

// Разобраться с модулями некоторые импортяться несколдько раз

const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,
  PipesModule,
  TextFieldModule,
  RouterModule,
  BrowserAnimationsModule
];
@NgModule({
  imports: [
    ...modules,
  ],
  declarations: [HeaderComponent, AvatarComponent],
  exports: [
    ...modules,
    HeaderComponent,
    AvatarComponent
  ]
})
export class SharedModule { }
