import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/users';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {

  @Input() user: User;
  @Input() size = 40;

  style = {};
  constructor() { }

  ngOnInit(): void {
    this.style = {
      width: this.size + 'px',
      height: this.size + 'px',
    };
  }

}
