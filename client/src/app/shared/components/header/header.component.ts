import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/components/auth/auth.service';
import { AddPostComponent } from 'src/app/components/add-post/add-post.component';
import { addPost } from 'src/app/store/actions/posts.actions';
import { formatISO } from 'date-fns';
import { StorageService } from 'src/app/services/storage.service';
import { ProfileComponent } from 'src/app/pages/profile/profile.component';
import { getAllUsers, updateUser } from 'src/app/store/actions/users.actions';
import { User } from 'src/app/models/users';
import { selectUserById } from 'src/app/store/selectors/users.selectors';
import { Router } from '@angular/router';

import { ModalService } from './../../../services/modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: User;
  isOpen = false;

  constructor(
    public authService: AuthService,
    private modalService: ModalService,
    private store: Store,
    private storageService: StorageService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.store.dispatch(getAllUsers()); // localstorage persist state?
    this.store.select(selectUserById, { id: this.storageService.getUserInfo().userId })
      .subscribe(user => this.user = user); // вынести в async ?? нет отписки
  }

  addPost(): void {
    this.modalService.openModal(AddPostComponent)
      .subscribe(postFormValues => {
        if (postFormValues) {
          const post = {
            ...postFormValues,
            date: formatISO(new Date()), // вынести в helper
            userId: this.storageService.getUserInfo().userId // переделать
          };
          this.store.dispatch(addPost({ post }));
        }
      });
  }

  goToUserProfile(): void {
    this.router.navigate(['/profile', this.user._id]);
  }

  toggleMenu(): void {
    this.isOpen = !this.isOpen;
  }
}
