import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials',
  pure: true,
})
export class InitialsPipe implements PipeTransform {
  transform(fullName: string): string {
    return fullName ? fullName.replace(/[^a-zA-Z- ]/g, '').match(/\b\w/g).join('') : '';
  }
}
