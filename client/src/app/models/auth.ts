export interface AuthData {
  email: string;
  password: string;
  name?: string;
  lastName?: string;
}

export interface UserInfo {
  token: string;
  userId: string;
}
