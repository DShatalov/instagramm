export interface SchemaResult<E> {
  result: string[];
  entities: E;
}
