export interface User {
  _id: string;
  avatar: File | string;
  name: string;
  lastName: string;
  fullName: string;
}

export interface NormalizedUsers {
  users: { [key: string]: User };
}
