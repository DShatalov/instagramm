import { User } from './users';

export interface Post {
  _id: string;
  userId: string;
  title: string;
  description: string;
  img: File | string;
  date: string;
  comments: string[];
}

export interface PostView extends Post {
  user: User;
}

export interface Comment {
  _id?: string;
  userId: string;
  postId: string;
  text: string;
  date: string;
  likes: number;
}

export interface NormalizedPosts {
  posts: { [key: string ]: Post };
  comments: { [key: string ]: Comment };
}
