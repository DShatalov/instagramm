export const getProp = (key: string): any => {
  try {
    return localStorage[key] ? JSON.parse(localStorage[key]) : null;
   /*  if (!resStr) {
      return null;
    }

    const res = JSON.parse(resStr);
    return res; */
  } catch (error) {
    return null;
  }
};

export const setProp = (key: string, value: any): void => {
  localStorage[key] = JSON.stringify(value);
};

export const removeProp = (key: string): void => {
  localStorage.removeItem(key);
};
