import * as keys from './keys';
import * as types from './types';
import { getProp, removeProp, setProp } from './actions';

export const setUserInfo = (userInfo: types.UserInfo) => {
  setProp(keys.userInfoKey, userInfo);
};

export const getUserInfo = () => {
  return getProp(keys.userInfoKey);
};

export const deleteUserInfo = () => {
  removeProp(keys.userInfoKey);
};
