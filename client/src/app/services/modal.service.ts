import { Observable } from 'rxjs';
import { Injectable, TemplateRef, Type } from '@angular/core';
// import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
// import { ComponentType } from '@angular/cdk/portal';
import { DialogService, DialogConfig } from '@ngneat/dialog';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor(private modal: DialogService) {}

  openModal<T>(component: Type<T> | TemplateRef<T>, config?: Partial<DialogConfig>): Observable<any>{
    const modal = this.modal.open(component, config);
    return modal.afterClosed$;
  }
}
