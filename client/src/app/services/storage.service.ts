import { Injectable } from '@angular/core';

import * as storage from '../utils/storage';
import * as StorageTypes from '../utils/storage/types';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  setUserInfo(value: StorageTypes.UserInfo): void {
    storage.setUserInfo(value);
  }

  getUserInfo(): StorageTypes.UserInfo {
    return storage.getUserInfo();
  }

  deleteUserInfo(): void {
    storage.deleteUserInfo();
  }
}
