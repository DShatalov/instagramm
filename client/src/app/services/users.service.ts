import { normalize } from 'normalizr';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { userSchema } from './../schema/user';
import { NormalizedUsers, User } from './../models/users';
import { UsersRequests } from '../requests/users';
import { SchemaResult } from '../models/schema';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private usersRequests: UsersRequests
  ) { }

  updateUser(user: User): Observable<User> {
    return this.usersRequests.updateUserRequest(user);
  }

  getAllUsers(): Observable<SchemaResult<NormalizedUsers>> {
    return this.usersRequests.getAllUsersRequest()
      .pipe(
        map(users => normalize(users, [userSchema]))
      );
  }

  getUser(id: string): Observable<User> {
    return this.usersRequests.getUserRequest(id);
  }
}
