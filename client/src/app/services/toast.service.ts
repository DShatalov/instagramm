import { Injectable } from '@angular/core';
import { DialogService } from '@ngneat/dialog';
import { ToastrService } from 'ngx-toastr';

// import { MatSnackBar } from '@angular/material/snack-bar';

export type ToastMessage = string;

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(private toastr: ToastrService) {}

  showSuccessToast(message: ToastMessage): void {
    this.toastr.success(message, 'Success');
  }

  showErrorToast(message: ToastMessage): void {
    this.toastr.error(message, 'Error');
  }
}
