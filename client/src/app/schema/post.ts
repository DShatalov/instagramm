import { schema } from 'normalizr';

const commentSchema = new schema.Entity('comments', {
}, { idAttribute: '_id' });


export const postSchema = new schema.Entity('posts', {
  comments: [commentSchema]
}, { idAttribute: '_id' });
