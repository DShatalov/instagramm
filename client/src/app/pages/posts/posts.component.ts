import { Post } from 'src/app/models/posts';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store/states/app.state';
import { Store } from '@ngrx/store';
import { getAllPosts } from 'src/app/store/actions/posts.actions';

import { getAllUsers } from './../../store/actions/users.actions';
import { selectAllPostsWithUsers } from './../../store/selectors/posts.selectors';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts$: Observable<Post[]>;
  postsWithUsers$: Observable<any>;

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(getAllPosts());
    this.postsWithUsers$ = this.store.select(selectAllPostsWithUsers);
  }
}
