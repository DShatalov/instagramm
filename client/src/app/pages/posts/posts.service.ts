import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { normalize } from 'normalizr';

import { SchemaResult } from './../../models/schema';
import { PostRequests } from '../../requests/posts';
import { NormalizedPosts, Post } from '../../models/posts';
import { postSchema } from '../../schema/post';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private postRequests: PostRequests,
  ) { }
  getAllPosts(): Observable<SchemaResult<NormalizedPosts>> {
    return this.postRequests.getAllPostsRequest()
      .pipe(
        map(posts => normalize(posts, [postSchema]))
      );
  }

  addPost(post: Post): Observable<Post> {
    return this.postRequests.addPostRequest(post)
      .pipe(map(this.mapToPost));
  }

  deletePost(id: string): Observable<string> {
    return this.postRequests.deletePostRequest(id);
  }

  // не нужен?
  mapToPost(post: Post): Post {
    const { _id, userId, title, description, img, date, comments } = post;
    return {
      userId,
      _id,
      title,
      description,
      img: `${environment.apiUrl}${img}`,
      date,
      comments
    };
  }
}
