import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { ComponentsModule } from './../components/components.module';
import { PostsComponent } from './posts/posts.component';
import { SharedModule } from '../shared/shared.module';
import { AuthGuard } from '../components/auth/auth.guard';

const routes: Routes = [
  {
    path: 'profile/:id',
    component: ProfileComponent,
    canActivate: [AuthGuard],
  }, {
    path: 'posts',
    component: PostsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [PostsComponent, ProfileComponent],
  imports: [RouterModule.forChild(routes), SharedModule, ComponentsModule],
  exports: [RouterModule]
})
export class PagesModule { }
