import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { User } from 'src/app/models/users';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { filter, switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { updateUser } from 'src/app/store/actions/users.actions';
import { StorageService } from 'src/app/services/storage.service';
import { AuthService } from 'src/app/components/auth/auth.service';

import { selectUserById } from './../../store/selectors/users.selectors';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
  // Погуглить как патчить форму без подписки
})
export class ProfileComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  user: User;
  isCurrentUserProfile = false;
  userSettingsForm = this.fb.group({
    name: [''],
    lastName: [''],
    avatar: ['']
  });
  constructor(
    private fb: FormBuilder,
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.activatedRoute.params
      .pipe(
        switchMap(params => this.store.select(selectUserById, { id: params.id })),
        filter(user => !!user)
      )
      .subscribe(user => {
        this.user = user;
        this.isCurrentUserProfile = this.authService.isCurrentUser(this.user._id);
        this.userSettingsForm.patchValue({
          name: user?.name,
          lastName: user?.lastName,
          avatar: user?.avatar
        });
      });
  }


  onSubmit(event): void {
    event.preventDefault();
    const fullName = `${this.userSettingsForm.value.name} ${this.userSettingsForm.value.lastName}`; // вынести
    const user = { ...this.userSettingsForm.value, _id: this.user._id, fullName };
    this.store.dispatch(updateUser({ user }));

  }

  onFileSelected(event): void {
    const files: FileList = event.target.files;
    const avatar = files.item(0);
    this.userSettingsForm.patchValue({ avatar });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
