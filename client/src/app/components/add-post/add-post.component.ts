import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DialogService, DialogRef } from '@ngneat/dialog';


@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddPostComponent implements OnInit {

  addPostForm = this.fb.group({
    title: ['', Validators.required],
    description: [''],
    img: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    public dialogRef: DialogRef
  ) { }

  ngOnInit(): void {
  }

  onSubmit(event): void {
    event.preventDefault();
    this.dialogRef.close(this.addPostForm.value);
  }

  onFileSelected(event): void {
    const files: FileList = event.target.files;
    const img = files.item(0);
    this.addPostForm.patchValue({ img });
  }
}
