import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommentsComponent } from './comments/comments.component';
import { CommentComponent } from './comments/comment/comment.component';
import { AddPostComponent } from './add-post/add-post.component';
import { PostComponent } from './post/post.component';
import { AuthModule } from './auth/auth.module';
import { SharedModule } from './../shared/shared.module';

const components = [
  PostComponent,
  AddPostComponent,
  CommentsComponent,
  CommentComponent,
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AuthModule,
  ],
  declarations: [
    ...components
  ],

  exports: [
    ...components
  ]
})
export class ComponentsModule { }
