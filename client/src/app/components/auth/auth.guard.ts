import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): true | UrlTree {
    return this.checkLogin();
  }

  checkLogin(): true | UrlTree {
    if (this.authService.isAuth) { return true; }

    // Redirect to the login page
    return this.router.parseUrl('/login'); // прочитать про метод parseUrl
  }
}
