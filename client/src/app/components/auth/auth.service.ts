import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { StorageService } from '../../services/storage.service';
import { AuthRequests } from '../../requests/auth';
import { AuthData, UserInfo } from '../../models/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth: boolean; // subject?

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authRequests: AuthRequests,
  ) { }

  register(authData: AuthData): Observable<string> {
    return this.authRequests.registerRequest(authData);
  }

  login(authData: AuthData): Observable<UserInfo> {
    return this.authRequests.loginRequest(authData);
  }

  logout(): void {
    this.storageService.deleteUserInfo();
    this.isAuth = false;
    this.router.navigate(['/login']);
  }

  isAuthenticated(): void { // не путаю ли я понятия авторизации и аутентификации?
    this.isAuth = this.storageService.getUserInfo() !== null;
  }

  getAuthToken(): string {
    return this.storageService.getUserInfo()?.token;
  }

  isCurrentUser(userId: string): boolean {
    return userId === this.storageService.getUserInfo().userId;
  }
}
