import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';
import { StorageService } from 'src/app/services/storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { MIN_PASSWORD_LENGTH } from '../constants';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // норм ли с ней работает?
})
export class LoginComponent implements OnInit {
  title = 'LOGIN';
  isLoginForm = true; // delete
  MIN_PASSWORD_LENGTH = MIN_PASSWORD_LENGTH;
  authForm: FormGroup;
  constructor(
    protected authService: AuthService,
    protected toastService: ToastService,
    protected storageService: StorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(MIN_PASSWORD_LENGTH)
      ])
    });
  }

  auth(): void {
    // console.log(this.authForm.value);
    this.authService.login(this.authForm.value).subscribe({
      next: userInfo => {
        this.authService.isAuth = true;
        this.storageService.setUserInfo(userInfo);
        this.router.navigate(['/posts']); // почитать документация про пути
      },
      error: error => this.toastService.showErrorToast(error.error)
    });
  }

  // Нужно как-то объеденить с registration
  requiredError(field: string): boolean {
    return this.authForm.get(field).touched && this.authForm.get(field).errors?.required ;
  }

  emailError(): boolean {
    return this.authForm.get('email').touched && this.authForm.get('email').errors?.email && !this.authForm.get('email').errors?.required;
  }

  passwordError(): boolean {
    return this.authForm.get('password').errors?.minlength;
  }
}
