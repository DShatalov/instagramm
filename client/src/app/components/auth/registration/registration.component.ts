import { Component, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';
import { StorageService } from 'src/app/services/storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { MIN_PASSWORD_LENGTH } from '../constants';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  title = 'REGISTRATION';
  isLoginForm = false;
  MIN_PASSWORD_LENGTH = MIN_PASSWORD_LENGTH;
  authForm: FormGroup;
  constructor(
    protected authService: AuthService,
    protected toastService: ToastService,
    protected storageService: StorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(MIN_PASSWORD_LENGTH)
      ]),
      name : new FormControl(null, [Validators.required]),
      lastName : new FormControl(null, [Validators.required]),
    });
  }

  auth(): void {
    const fullName = `${this.authForm.value.name} ${this.authForm.value.lastName}`; // вынести
    const registrationData = { ...this.authForm.value, avatar: null, fullName }; // Может аватра на бэке по дефолну создать можно?
    this.authService.register(registrationData).subscribe({
      next: val => {
        this.toastService.showSuccessToast(val);
        this.router.navigate(['/login']); // тут ли редиректить? сделать unsbscribe
      },
      error: error => this.toastService.showErrorToast(error.error)
    });
  }

  requiredError(field: string): boolean {
    return this.authForm.get(field).touched && this.authForm.get(field).errors?.required;
  }

  emailError(): boolean {
    return this.authForm.get('email').touched && this.authForm.get('email').errors?.email && !this.authForm.get('email').errors?.required;
  }

  passwordError(): boolean {
    return this.authForm.get('password').errors?.minlength;
  }
}
