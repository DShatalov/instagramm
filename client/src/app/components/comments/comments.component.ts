import { formatISO } from 'date-fns';
import { Observable } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { addComment } from 'src/app/store/actions/posts.actions';
import { Store } from '@ngrx/store';
import { StorageService } from 'src/app/services/storage.service';
import { selectCommentsByPostId, selectCommentsWithUsersByPostId } from 'src/app/store/selectors/posts.selectors';
import { User } from 'src/app/models/users';

import { Comment } from './../../models/posts';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() postId: string;
  commentsWithUsers$: Observable<Array<Comment & { user: User }>>; // вынести в отдельный интерфейс
  comment: string;
  constructor(
    private store: Store,
    private storageService: StorageService,
  ) { }

  ngOnInit(): void {
    this.commentsWithUsers$ = this.store.select(selectCommentsWithUsersByPostId, { id: this.postId });
  }

  send(text: string): void { // add enter hotkey
    const comment = {
      text,
      postId: this.postId,
      userId: this.storageService.getUserInfo().userId,
      date: formatISO(new Date()),
      likes: 0
    };
    this.store.dispatch(addComment({ comment }));
    this.comment = '';
  }
}
