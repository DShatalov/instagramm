import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { ChangeDetectionStrategy, Component, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { deleteComment, updateComment } from 'src/app/store/actions/posts.actions';
import { Comment } from 'src/app/models/posts';
import { User } from 'src/app/models/users';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentComponent implements OnInit {
  @Input() comment: Comment & { user: User };
  @Input() postId: string;
  isEdit = false;
  text = '';
  isCurrentUserComment = false;
  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  constructor(
    private store: Store,
    private ngZone: NgZone,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.isCurrentUserComment = this.authService.isCurrentUser(this.comment.user._id);
  }

  deleteComment(commentId: string): void {
    this.store.dispatch(deleteComment({ commentId, postId: this.postId }));
  }

  editComment(text: string): void {
    const comment = { text, postId: this.postId, _id: this.comment._id } as Comment;
    this.store.dispatch(updateComment({ comment }));
    this.isEdit = false;
  }

  enableEdit(): void {
    this.isEdit = true;
    this.text = this.comment.text;
    this.triggerResize();
  }

  triggerResize(): void {
    this.ngZone.onStable.pipe(take(1))
      .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  goToUserProfile(): void {
    this.router.navigate(['/profile', this.comment.user._id]);
  }

  get shouldShowEditButtons(): boolean {
    return !this.isEdit && this.isCurrentUserComment;
  }
}
