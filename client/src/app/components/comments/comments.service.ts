import { Comment } from 'src/app/models/posts';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CommentsRequests } from './../../requests/comments';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(
    private commentsRequests: CommentsRequests
  ) { }


  addPost(comment: any): Observable<any> { // type and rename
    return this.commentsRequests.addPostRequest(comment);
  }

  deleteComment(commentId: string, postId: string): Observable<{ commentId: string, postId: string}> {
    return this.commentsRequests.deleteCommentRequest(commentId, postId);
  }

  updateComment(comment: Comment): Observable<Comment> {
    return this.commentsRequests.updateCommentRequest(comment);
  }
}
