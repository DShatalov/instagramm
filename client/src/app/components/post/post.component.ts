import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { deletePost } from 'src/app/store/actions/posts.actions';
import { selectCommentsByPostId } from 'src/app/store/selectors/posts.selectors';
import { Router } from '@angular/router';

import { PostView } from './../../models/posts';
import { Comment } from './../../models/posts';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit {

  @Input() post: PostView;
  comments$: Observable<Comment[]>;
  constructor(
    private store: Store,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  deletePost(id: string): void {
    this.store.dispatch(deletePost({ id }));
  }

  goToUserProfile(): void {
    this.router.navigate(['/profile', this.post.user._id]);
  }
}
