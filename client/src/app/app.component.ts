import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from './components/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  title = 'client';
  apiUrl = 'http://localhost:5000/api/posts/add';
  constructor(
    private httpClient: HttpClient,
    private translate: TranslateService,
    public authService: AuthService
  ) {
    translate.addLangs(['en']);
    translate.setDefaultLang('en');
    translate.use('en');
  }
  ngOnInit(): void {
    this.authService.isAuthenticated();
  }

/*   myUploader(event): void {
    const formData: FormData = new FormData();
    const data = event[0];
    const test = {
      title: 'wtf',
      description: 'wtf23123',
      userId: '5e147b629d898047e40e904c',
      date: new Date().getTime(),
    };
    console.log(data)
    formData.append('postImage', data);
    formData.append('data', JSON.stringify(test));
    this.httpClient.post('http://localhost:5000/api/posts/add', formData)
      .subscribe(x => console.log(x));
  } */
}
