import { StorageService } from 'src/app/services/storage.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Comment } from 'src/app/models/posts';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsRequests {
  baseUrl = `${environment.apiUrl}api/comments/`;
  constructor(
    private httpClient: HttpClient,
    private storageService: StorageService
  ) { }

  addPostRequest(comment: any): Observable<any> {
    const url = `${this.baseUrl}add`;
    const userId = this.storageService.getUserInfo().userId;
    return this.httpClient.post<any>(url, { ...comment, userId });
  }

  deleteCommentRequest(commentId: string, postId: string): Observable<{ commentId: string, postId: string}> {
    const url = `${this.baseUrl}delete`;
    return this.httpClient.post<{ commentId: string, postId: string}>(url, { commentId, postId });
  }

  updateCommentRequest(comment: Comment): Observable<Comment> {
    const url = `${this.baseUrl}update`;
    return this.httpClient.post<Comment>(url, comment);
  }
}
