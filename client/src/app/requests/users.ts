import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { User } from './../models/users';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersRequests {
  baseUrl = `${environment.apiUrl}api/users/`;
  constructor(
    private httpClient: HttpClient,
    )
    {}
  updateUserRequest(user: any): Observable<any> {
    const url = `${this.baseUrl}update`;

    const formData: FormData = new FormData();
    const { avatar, ...data } = user;
    formData.append('avatar', avatar);
    formData.append('data', JSON.stringify(data));

    return this.httpClient.post<User>(url, formData);
  }

  getAllUsersRequest(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.baseUrl);
  }

  getUserRequest(id: string): Observable<User> {
    const url = `${this.baseUrl}${id}`;
    return this.httpClient.get<User>(url);
  }
}
