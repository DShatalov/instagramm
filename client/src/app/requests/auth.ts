import { HttpClient } from '@angular/common/http';
import { UserInfo } from '../models/auth';
import { AuthData } from '../models/auth';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthRequests {
  apiUrl = environment.apiUrl;
  constructor(private httpClient: HttpClient) {
  }
  registerRequest(authData: AuthData): Observable<string> {
    const url = `${this.apiUrl}api/auth/register`;
    return this.httpClient.post<string>(url, authData);
  }

  loginRequest(authData: AuthData): Observable<UserInfo> {
    const url = `${this.apiUrl}api/auth/login`;
    return this.httpClient.post<UserInfo>(url, authData);
  }
}
