import { tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';

import { AuthService } from '../../components/auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private toastService: ToastService,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.authService.getAuthToken();

    if (!authToken) {
      return next.handle(req);
    }

    const authReq = req.clone({
      headers: req.headers.set('Authorization', authToken)
    });

    // Todo handle errors
    return next.handle(authReq).pipe(
      tap(
        event => {},
        err => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.toastService.showErrorToast(err.error);
              this.authService.logout();
            }
          }
        }
      )
    );
  }
}
