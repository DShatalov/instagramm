import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Post } from '../models/posts';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostRequests {
  baseUrl = `${environment.apiUrl}api/posts/`;
  constructor(
    private httpClient: HttpClient,
    )
    {}
  addPostRequest(postData: Post): Observable<Post> {
    const url = `${this.baseUrl}add`;

    const formData: FormData = new FormData();
    const { img, ...data } = postData;
    formData.append('postImage', img);
    formData.append('data', JSON.stringify(data));

    return this.httpClient.post<Post>(url, formData);
  }

  getAllPostsRequest(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.baseUrl);
  }

  getPostRequest(id: string): Observable<Post> {
    return this.httpClient.get<Post>(this.baseUrl);
  }

  deletePostRequest(id: string): Observable<string> {
    const url = `${this.baseUrl}${id}`;
    return this.httpClient.delete<string>(url);
  }
}
