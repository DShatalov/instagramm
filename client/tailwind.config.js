module.exports = {
  purge: [],
  theme: {
    extend: {
      width: {
        '14/15': '93.3%',
        '16/18': '88.8%',
      },
    },
    container: {
      center: true,
    },
    stroke: {
      current: 'currentColor',
      white: '#fff',
    },
    fill: {
      current: 'currentColor',
      white: '#fff',
    },
  },
  variants: {
  },
  plugins: [],
  future: {
    removeDeprecatedGapUtilities: true,
  },
};
