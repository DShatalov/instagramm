const { Router } = require('express');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');
const responses = require('./responses.json');
const checkToken = require('../middlewares/auth');
const Post = require('../models/Post');

const router = Router();


// /api/comments
router.post(
  '/add',
  checkToken,
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: responses.DEFAULT_ERROR,
        });
      }

      const comment = { ...req.body, _id: new mongoose.Types.ObjectId() };
      await Post.update(
        { _id: req.body.postId },
        { $push: { comments: comment } },
      );
      return res.status(201).json(comment);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.post(
  '/update',
  checkToken,
  async (req, res) => {
    try {
      const post = await Post.findOneAndUpdate(
        { _id: req.body.postId, 'comments._id': req.body._id },
        { $set: { 'comments.$.text': req.body.text } },
        { new: true },
      );
      const updatedComment = post.comments.filter((comment) => String(comment._id) === req.body._id)[0];
      return res.status(201).json(updatedComment);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.post(
  '/delete',
  checkToken,
  async (req, res) => {
    try {
      const { commentId, postId } = req.body;
      await Post.update(
        { _id: postId, 'comments._id': commentId },
        { $pull: { comments: { _id: commentId } } },
      );
      return res.status(201).json(req.body);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

module.exports = router;
