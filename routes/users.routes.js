const { Router } = require('express');
const { validationResult } = require('express-validator');

const multer = require('multer');
const responses = require('./responses.json');
const checkToken = require('../middlewares/auth');
const User = require('../models/User');

const router = Router();

// SET STORAGE
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads/users');
  },
  filename(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({ storage });
// /api/users/
router.post(
  '/update',
  checkToken,
  upload.single('avatar'),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: responses.DEFAULT_ERROR,
        });
      }
      const { _id, name, lastName, fullName } = JSON.parse(req.body.data);
      const avatar = req.file ? req.file.path : req.body.avatar;

      const user = await User.findOneAndUpdate(
        { _id },
        { name, lastName, avatar, fullName },
        { new: true },
      );
      const { password, ...resp } = user.toObject();
      return res.status(201).json(resp);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.get(
  '',
  checkToken,
  async (req, res) => {
    try {
      const user = await User.find().select('-password');
      return res.status(201).json(user);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.get(
  ':/id',
  checkToken,
  async (req, res) => {
    try {
      const { _id } = req.params;
      const user = await User.findById(_id);
      return res.status(201).json(user);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);
module.exports = router;
