const { Router } = require('express');
const { validationResult } = require('express-validator');
const multer = require('multer');
const responses = require('./responses.json');
const checkToken = require('../middlewares/auth');
const Post = require('../models/Post');

const router = Router();

// SET STORAGE
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './uploads/posts');
  },
  filename(req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({ storage });
// /api/posts
router.post(
  '/add',
  checkToken,
  upload.single('postImage'),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: responses.DEFAULT_ERROR,
        });
      }
      const { title, description, userId, date } = JSON.parse(req.body.data);
      const img = req.file.path;
      const post = new Post({ userId, title, description, img, date });

      await post.save();

      return res.status(201).json(post);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.get(
  '',
  checkToken,
  async (req, res) => {
    try {
      const post = await Post.find();
      console.log(post);
      return res.status(201).json(post);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.delete(
  '/:id',
  checkToken,
  async (req, res) => {
    try {
      const { id } = req.params;
      await Post.findOneAndDelete({ _id: id });
      return res.status(201).json(id);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

module.exports = router;
