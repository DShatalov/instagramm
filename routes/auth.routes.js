const { Router } = require('express');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const responses = require('./responses.json');
const User = require('../models/User');

const router = Router();

// /api/auth
router.post(
  '/register',
  [
    check('email', responses.INCORRECT_EMAIL).isEmail(),
    check('password', responses.WEAK_PASSWORD).isLength({ min: 6 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
          message: responses.INCORRECT_REGISTRATION_DATA,
        });
      }
      const { email, password, name, avatar, lastName, fullName } = req.body; // может убрать деструктуризацию?
      const candidate = await User.findOne({ email });

      if (candidate) {
        return res.status(400).json(responses.USER_ALREADY_EXISTS);
      }

      const hashedPassword = await bcrypt.hash(password, 12);
      const user = new User({ email, password: hashedPassword, name, avatar, lastName, fullName });

      await user.save();

      return res.status(201).json(responses.USER_CREATED);
    } catch (e) {
      return res.status(500).json(responses.DEFAULT_ERROR);
    }
  },
);

router.post(
  '/login',
  [
    check('email', responses.INCORRECT_EMAIL).normalizeEmail().isEmail(),
    check('password', responses.EMPTY_PASSWORD).exists(),
  ],
  async (req, res) => {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email });

      if (!user) {
        return res.status(400).json(responses.USER_NOT_FOUND);
      }

      const isPasswordMatch = await bcrypt.compare(password, user.password);

      if (!isPasswordMatch) {
        return res.status(400).json(responses.INCORRECT_PASSWORD);
      }

      const token = jwt.sign(
        { userId: user.id },
        config.get('jwtSecretKey'),
        { expiresIn: '24h' },
      );

      return res.json({ token, userId: user.id });
    } catch (e) {
      return res.status(500).json({ message: responses.DEFAULT_ERROR });
    }
  },
);

module.exports = router;
