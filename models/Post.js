const { Schema, model, Types } = require('mongoose');

const schema = new Schema({
  userId: Types.ObjectId,
  title: { type: String, required: true },
  description: String,
  img: String,
  date: String,
  comments: [{
    text: String,
    userId: Types.ObjectId,
    postId: Types.ObjectId,
    likes: Number,
    usersIdLikes: [String],
    date: String,
  }],
});

module.exports = model('Post', schema);
