module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'linebreak-style': ['error', process.env.NODE_ENV === 'prod' ? "unix" : "windows"],
    'no-console': 'off',
    'object-curly-newline': 'off',
    "consistent-return": "off",
    "no-underscore-dangle": "off",
    'max-len': ["error", { "code": 120 }]
  },
};