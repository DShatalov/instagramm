const jwt = require('jsonwebtoken');
const config = require('config');
const responses = require('../routes/responses.json');

const checkToken = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers.authorization;
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.get('jwtSecretKey'), (err, decoded) => {
      if (err) {
        return res.status(401).json(responses.AUTH_TOKEN_IS_EXPIRED);
      }
      req.decoded = decoded;
      next();
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied',
    });
  }
};

module.exports = checkToken;
